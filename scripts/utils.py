import itertools
import nltk
import numpy as np
import gensim
import random

from sklearn.datasets import fetch_20newsgroups
from nltk.tokenize import sent_tokenize

from gensim.utils import simple_preprocess
import file_handling as fh

def simple_preprocess_and_remove_stopwords(text, stopwords = None):
	if stopwords != None:
		return [token for token in simple_preprocess(text) if token not in stopwords]
	else:
		return [token for token in simple_preprocess(text)]

def get_mallet_stopwords():
	mallet_stopwords = fh.read_text('../../data/mallet_stopwords.txt')
	return {s.strip() for s in mallet_stopwords}

def iter_texts(texts, stopwords):
	for text in texts:
			tokens = simple_preprocess_and_remove_stopwords(text, stopwords)
			yield tokens
		
def preprocess_texts(texts, stopwords):
	return (tokens for tokens in iter_texts(texts, stopwords))
	
def get_mm_corpus(corpus):
	gensim.corpora.MmCorpus.serialize('./bow.mm', corpus)
	return gensim.corpora.MmCorpus('./bow.mm')
	
def prepare_lda_model(texts, stopwords=None, num_topics=50, passes = 100):
	doc_stream = preprocess_texts(texts, stopwords)
	id2word = gensim.corpora.Dictionary(doc_stream)
	print(id2word)
	corpus = CorpusIterator(texts, id2word, stopwords)
	print("Preparing corpus")
	corpus = get_mm_corpus(corpus)
	print("Building LDA model")
	return gensim.models.LdaMulticore(corpus, num_topics=num_topics, id2word=id2word, passes=passes)
	
def without_kth(elements, k):
    return elements[:k] + elements[k+1:]

def merge_lists(elements):
    return {item for sublist in elements for item in sublist}

def write_topics_to_file(file_name, topics):
    topics_file = open(file_name, 'w')
    for topic in topics:
        topics_file.write(' '.join(topic))
        topics_file.write('\n')
    topics_file.close()
    
def save_topics_with_intruders(topic_words, file_name):
    intruders_file = open(file_name + '_intruders.txt', 'w')
    topics_with_intruders_file = open(file_name + '_with_intruders.txt', 'w')
    for i in range(0,len(topic_words)):
        ith = set(topic_words[i])
        other = merge_lists(without_kth(topic_words, i)) - ith
        intruder = list(other)[random.randint(0, len(other))]
        index = random.randint(0, len(ith))
        ith = list(ith)
        ith.insert(index, intruder)
        topics_with_intruders_file.write(' '.join(ith) + '\n')
        intruders_file.write(str(index + 1) + '\n')
    topics_with_intruders_file.close()
    intruders_file.close()
    
def save_corpus(file_name, texts, stopwords):
    corpus_file = open(file_name, 'w', encoding="utf-8")
    corpus_file.write('\n'.join([' '.join(simple_preprocess_and_remove_stopwords(text.replace('\n', ' '), stopwords)) for text in texts]))
    corpus_file.close()

class CorpusIterator(object):
	def __init__(self, texts, dictionary, stopwords, clip_docs=None):
		self.texts = texts
		self.dictionary = dictionary
		self.stopwords = stopwords
		self.clip_docs = clip_docs

	def __iter__(self):
		for tokens in itertools.islice(iter_texts(self.texts, self.stopwords), self.clip_docs):
			yield self.dictionary.doc2bow(tokens)

	def __len__(self):
		return self.clip_docs
		
		

		
		
print("loaded utils.py")

	