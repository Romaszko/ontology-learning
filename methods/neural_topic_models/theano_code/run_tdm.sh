export MKL_THREADING_LAYER=GNU
export THEANO_FLAGS='device=cuda,force_device=True'

python run_ngtm.py ../data/20ng/20ng_all train output --test test --encoder_layers 1 --encoder_shortcut --generator_layers 4 --generator_shortcut --train_bias --l1_penalty 0.01 --sparsity_target 0.1
